//
//  main.m
//  Luchin24
//
//  Created by Ученик on 02.12.16.
//  Copyright (c) 2016 Учитель. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
